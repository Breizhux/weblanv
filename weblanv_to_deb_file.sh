source_dir="weblanv"
destination="weblanv.deb"
#username="user"
username=$(ls /home/ | grep [^lost+found])

#On supprime le .deb existant
if [ -e "$destination" ]; then
    rm "$destination"
fi
#On met les droits root sur le source_dir
sudo chown -R root:root "$source_dir/"
#On met les bons droits sur les fichiers DEBIAN
sudo chmod -R 755 "$source_dir/DEBIAN/"
#On créer le fichier deb d'installation
sudo dpkg-deb --build "$source_dir/"
#On donne les droits à l'utilisateur sur .deb créé (sinon c'est pas très sympa pour lui)
sudo chown "$username" "$destination"
sudo chgrp "$username" "$destination"
sudo chmod 755 "$destination"
#On remet les droits au dossier source_dir
sudo chown -R "$username:$username" "$source_dir/"
