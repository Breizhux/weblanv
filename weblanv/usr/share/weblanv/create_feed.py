#!/usr/bin/env python3
#coding: utf-8

import os
import requests
import facebook_scraper
from pytz import UTC
from feedgen.feed import FeedGenerator
try :
    from transformers import pipeline
    SUMMARIZER = pipeline("summarization", model="t5-base", tokenizer="t5-base")
except :
    SUMMARIZER = None

                                            ##########################################
                                            ### COMMON PART FOR ALL SOCIAL NETWORK ###
                                            ##########################################

def get_feed(source, url, nbpage=4) :
    """ Return the feed from anyway social network"""
    if source == "facebook" :
        page_fb_id = get_true_fb_id(url)
        print("[facebook] create feed for {}".format(page_fb_id))
        return get_facebook_feed(url, page_fb_id, nbpage)
    elif source == "twitter" :
        print("[twitter] create feed for {}".format(url))

def get_always(text, starttext, endtext="\"") :
    """ Retourne ce qui se trouve entre starttext et
    endtext dans le text donné"""
    s = text.index(starttext)+len(starttext)
    e = text.index(endtext, s)
    while starttext in text[s:e] :
        s = text.index(starttext, s) + len(starttext)
    return text[s:e]

def get_title(text) :
    """ return a title from a text"""
    if len(text) < 47 : return text
    elif SUMMARIZER is None :
        ponctuation = ",.!?"
        i=0
        for i, c in enumerate(text) :
            if c in ponctuation :
                end = i
                break
        if i != 0 : title = text[:i+1]
        else : title = text
    else :
        title = SUMMARIZER(text, min_length=5, max_length=20)
    return title

def get_summary(text) :
    """ Return a summary of text """
    if len(text) < 150 :
        return text
    elif SUMMARIZER is None :
        return text[:150]+"... read more in content"
    return SUMMARIZER(text, min_length=40, max_length=170)

def get_code_for_image(post_url, imgs) :
    """ Return the RSS code for include image
    in RSS content of an article"""
    images_code = '<p><img src="{url}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" data-permalink="{post_url}" data-orig-file="{url}" data-image-title="No title for image" data-medium-file="{url}" data-large-file="{url}"></p>'
    text = ""
    if imgs is not None :
        text += "<![CDATA["
        for i in imgs :
            text += images_code.format(url=i, post_url=post_url)
    text += "<br>"
    return text


                                        #################################################
                                        ### FUNCTIONS FOR FACEBOOK WEBPAGE CONVERSION ###
                                        #################################################
                                        # scraper : https://github.com/kevinzg/facebook-scraper

def get_facebook_feed(url, fb_id, nbpage) :
    """ Return facebook feed """
    #Create the Feed object
    feed = FeedGenerator()
    feed.id(url)
    feed.title('RSS Facebook feed')
    feed.author({'name' : 'WebLanv'})
    feed.link(href=url, rel="alternate")
    feed.logo("https://static.xx.fbcdn.net/rsrc.php/yb/r/hLRJ1GG_y0J.ico")
    feed.description('RSS Facebook feed create from {}'.format(url))
    feed.load_extension('media', atom=True)
    # extract and add post in feed object
    posts = facebook_scraper.get_posts(fb_id, pages=nbpage)
    for post in posts :
        item = feed.add_entry()
        #Complete the feed with informations
        item.id(post['post_id'])
        item.link(href=post['post_url'], rel="alternate")
        item.title(get_title(post['text']))
        item.author(name=post['username'], email="https://www.facebook.com/{}".format(post['user_id']))
        item.pubDate(post['time'].replace(tzinfo=UTC))
        # Get code for integrates all images in content feed
        images = post['images_lowquality']
        if post['image_lowquality'] is not None and not post['image_lowquality'] in images :
            images.append(post['image_lowquality'])
        image_code = get_code_for_image(post['post_url'], images)
        #Add content with media in the content
        item.content(image_code + post['text'])

    return feed.rss_str(pretty=True)

def get_true_fb_id(url) :
    """ Return the true id of the facebook webpage"""
    if "/groups" in url :
        page = requests.get(url)
        return get_always(page.text, 'content="fb://group/?id=')
    else :
        return url[url[:-1].rfind('/')+1:].strip('/')

                                        ################################################
                                        ### FUNCTIONS FOR TWITTER WEBPAGE CONVERSION ###
                                        ################################################
                                        # scraper : https://github.com/bisguzar/twitter-scraper


if __name__ == "__main__" :
    url = "https://www.facebook.com/groups/hackathonhackers/"
    feed = get_feed("facebook", url)#, nbpage=7)
    print(feed)
    with open("my_rss_feed.xml", 'w') as file :
        file.write(feed)
