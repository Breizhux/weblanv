#!/usr/bin/env python3
#coding: utf-8

import sys
import create_feed
from webfunctions import *
from flask import Flask, render_template, redirect, url_for, request, make_response

app = Flask(__name__)

                                                    #################################
                                                    ### WEB PAGE FOR GET FEED URL ###
                                                    #################################

TEXTE_TO_APPEND = "entrez le lien du compte que vous voulez suivre !"
URL_TO_APPEND = ""

@app.route('/')
def home() :
    global TEXTE_TO_APPEND, URL_TO_APPEND
    return render_template('home.html', message=TEXTE_TO_APPEND, url=URL_TO_APPEND)
@app.route('/get_url_feed', methods=['POST'])
def get_url_feed() :
    global TEXTE_TO_APPEND, URL_TO_APPEND
    try :
        URL_TO_APPEND = get_feeded_url(request.host_url, request.form['urltorss'])
        TEXTE_TO_APPEND = ""
    except :
        if not request.form['urltorss'] == "" :
            TEXTE_TO_APPEND = "lien invalide !"
    return redirect(url_for('home'))


                                                    ################################
                                                    ### BACKEND FOR GET RSS FEED ###
                                                    ################################

@app.route('/feed/<source>/<url>')
def rss(source, url):
    url = get_src_url(source, url)
    #get parameter in url
    page = request.args.get("page", default=4, type=int)
    #generate feed and create response
    generate_feed = create_feed.get_feed(source, url, nbpage=page)
    response = make_response(generate_feed)
    response.headers.set('Content-Type', 'application/rss+xml')
    return response

if __name__ == "__main__" :
    #app.run(host="192.168.201.228")
    app.run()
