#!/usr/bin/env python3
#coding: utf-8

from urllib.parse import urlparse

BASE_RSS_URL = "feed/"
BASE_URL = {
    'facebook' : 'https://www.facebook.com/',
}

def get_feeded_url(host, url) :
    """ Return the feeded url """
    return "{}{}{}/{}".format(host,
                              BASE_RSS_URL,
                              get_source_site(url),
                              get_path(url))

def get_source_site(url) :
    """ Return the name of principal site """
    source_site = urlparse(url).hostname
    source_site = source_site[:source_site.rfind('.')]
    source_site = source_site[source_site.rfind('.')+1:]
    return source_site

def get_path(url) :
    """ Return the rests of url important to
    find the source of website"""
    return urlparse(url).path.strip('/').replace('/', '--')

def get_src_url(src, url) :
    """ Return the true url """
    return BASE_URL[src] + url.replace('--', '/')
