# WebLanv

WebLanv est un serveur qui convertit une page publique d'un réseau social en flux RSS.

##### Sites disponible :
- Facebook

##### Sites prévue à l'avenir :
- Twitter


## Installation

Sur un système Debian, en root :

```sh
apt install -y git
git clone https://gitlab.com/Breizhux/weblanv.git
cd weblanv
chmod +x "weblanv_to_deb_file.sh"
./webweblanv_to_deb_file.sh
apt install ./weblanv.deb
```

Questions lors de l'installation :<br>
→ Faut-il installer la bibliothèque transformers (et le modèle t5-base pré-entraîné de google), afin de générer de [meilleurs titres](#les-titres-darticles).<br>
→ Quel nom de domaine sera utilisé pour l'installation de WebLanv.

La configuration en HTTPS n'a pas encore été testé, mais devrait normalement fonctionner.


## Fonctionnement

Le projet est fait en deux parties :

- L'interface web : permet de récupérer un lien RSS pour la page souhaitée.
- Le convertisseur : retourne le flux RSS correspondant à la page suivit.

### Technique

Le projet est écrit en python afin d'utiliser des scrapers très régulièrement mis à jour. L'interface web utilise Flask afin d'être mise en lien très facilement avec les scrapers, tout en restant léger.

La partie serveur web fonctionne en deux parties :<br>
- la racine (/) : permet d’accéder à l'interface web.<br>
- le flux (/feed) : permet d'appeler le convertisseur de pages en flux RSS.

### Interface Web

Les liens sont totalement prédictibles et réversibles, cela afin de ne pas recourir à l'utilisation d'une base de donnée pour chaque page.

Syntaxe : http(s)://\<hostname\>/feed/\<site\>/\<reste du chemin dans l'url en remplaçant les "/" par deux "-"\>

Exemple :<br>
→ le groupe facebook : https://www.facebook.com/groups/GNUAndLinux/<br>
→ aura comme flux RSS : http://localhost/feed/facebook/groups- -GNUAndLinux

**Il est possible de passer des options dans l'url du flux RSS :**

| Otions | Sites pour lequel l'option est disponible | Utilité | Valeur par défaut |
| :----: | :---------------------------------------: | :-----: | :---------------: |
| page | Facebook, | Nombre de page à charger | 4 |

### Convertisseur

1 - Le convertisseur appel le bon scraper pour récupérer les informations pour chaque post de la page suivit :

| Sites | Nom du scraper | Dépôt du scraper |
| :---: | :------------: | :--------------- |
| Facebook | facebook-scraper | https://github.com/kevinzg/facebook-scraper |
| Twitter | twitter-scraper | https://github.com/kennethreitz/twitter-scraper |

2 - Les informations sont mises dans un flux RSS construit à l'aide de la bibliothèque FeedGen : https://github.com/lkiesow/python-feedgen

Presque aucune présentation n'est faite dans le flux RSS pour le moment. Seulement l'intégration des images trouvées dans le contenu du flux RSS.


#### Les titres d'articles

Il n'y a pas de titre dans les posts facebook ou twitter. Pour les créer, il est possible d'installer python-transformers, ou de faire un coupage brut du texte :

**Si la bibliothèque transformers n'est pas installé,** le titre est trouvé en prenant le début du message jusqu'à la première ponctuation (si le texte est suffisamment long).

**Si la bibliothèque transformers a été installé,** la bibliothèque [transformers](https://github.com/huggingface/transformers) donne la possibilité de résumer un texte. En l'installant, elle sera utilisée pour trouver un titre si le texte est assez long. Mais cela est évidemment beaucoup plus lourd et plus lent, mais peut-être de meilleure qualité qu'un découpage brut. Son utilisation n'est bien sûr pas obligatoire.